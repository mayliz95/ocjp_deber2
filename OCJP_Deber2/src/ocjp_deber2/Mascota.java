/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocjp_deber2;

/**
 *
 * @author Mayra
 */
public class Mascota {
 
    int edad;
    String nombre;
    String raza;

    public Mascota() {
    }

    public Mascota(int edad, String nombre, String raza) {
        this.edad = edad;
        this.nombre = nombre;
        this.raza = raza;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    @Override
    public String toString() {
        return "Mascota -----------" + "\nEdad=" + edad + "\nNombre=" + nombre + "\nRaza=" + raza;
    }
           
}
