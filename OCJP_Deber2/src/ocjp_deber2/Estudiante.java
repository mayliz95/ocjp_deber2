/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocjp_deber2;

import javax.swing.JOptionPane;

/**
 *
 * @author Mayra
 */
public class Estudiante extends Persona {

    double notaPromedio;
    int numMaterias;

    public Estudiante() {
    }

    public Estudiante(int numMaterias, String nombre, String apellido, int edad, String CI, Mascota mascot) {
        super(nombre, apellido, edad, CI, mascot);        
        this.numMaterias = numMaterias;
    }
                  
    @Override
    public String pagoMatricula() {
        
        return "El estudiante debe pagar: " + (numMaterias * 100);
    }     
    
    public void ingresarNumMaterias( int numero) {
        numMaterias = numero;
    }
    
    public void ingresarNumMaterias() {
        int numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número de materias que toma"));
        numMaterias = numero;
    }
    
    public void calcularPrmedio(){       
        
        int sumaNotas = 0;
        for (int i = 0; i < numMaterias; i++) {
            int dato = Integer.parseInt(JOptionPane.showInputDialog("Inreges la nota de la materia Nro: " + (i+1)));
            sumaNotas += dato;
        }      
        
        notaPromedio = sumaNotas / numMaterias;
    }
    
    @Override
    public void entrena(String Deporte) {
        String salida ="";
        salida+="La persona entrena " + Deporte;
        salida += "\nLos dias: ";        
        salida += Dias.MIERCOLES.toString() + "\n" + Dias.SABADO.toString();
        JOptionPane.showMessageDialog(null,salida );
    }

    @Override
    public String toString() {
        
        return super.toString() + "\nNota Promedio=" + notaPromedio + "\nNumero de Materias=" + numMaterias + '}';
    }
    
    
}
