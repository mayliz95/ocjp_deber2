/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocjp_deber2;

import javax.swing.JOptionPane;

/**
 *
 * @author Mayra
 */
public abstract class Persona implements Deportista {
    
    String nombre;
    String apellido;
    int edad;
    String CI;
    Mascota mascota;

    public Persona() {
    }

    public Persona(String nombre, String apellido, int edad, String CI, Mascota mascot) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.CI = CI;
        this.mascota = mascot;
    }
    
    public abstract String pagoMatricula();    

    @Override
    public String toString() {
        return "Nombre: " + nombre + " " + apellido + "\nEdad: " + edad +"\nCI: " + CI + "\n" + mascota.toString();
    }   
}
